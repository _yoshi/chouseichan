# heroku


## deploy

`git push`でデプロイする方法以外に、maven pluginでデプロイする方法がある。

https://devcenter.heroku.com/articles/deploying-java-applications-with-the-heroku-maven-plugin

gitでデプロイだとmavenのプロファイルを明示的に指定できない？ので、
プラグインでデプロイする方法にした。

```sh
mvn -P production clean heroku:deploy
```

### なぜプロファイルが必要か？

npmでビルドしたファイルをSpring Bootのjarにパッケージするようにしている。
（単一の実行可能jarでherokuにデプロイするため）

普段の`mvn compile`などで、`npm build`等が実行されるとビルドに時間がかかってしまうため、
`production`プロファイルでのみ、`npm build`やリソースのコピーが行われるようにした。

### ハマりポイント

mavenプラグインでデプロイすると、`git push`したときと異なり、`./mvnw`の起動に失敗する（ファイルが存在しない）。

```sh
/bin/sh: 1: ./mvnw: not found
```

もともと、proc type `release` でflywayのmigrationを実行していたが、
このエラーのせいでデプロイに失敗してしまった。

Spring Bootにflywayを組み込む機能があるので、そちらを使うようにして
アプリケーション起動時にmigrationが実行されるようにした。
結果としてproc type `release` は不要になったため、上記のエラーも解消された。


 