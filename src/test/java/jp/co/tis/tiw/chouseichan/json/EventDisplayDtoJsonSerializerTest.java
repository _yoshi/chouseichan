package jp.co.tis.tiw.chouseichan.json;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import jp.co.tis.tiw.chouseichan.dto.DataSourceDto;
import jp.co.tis.tiw.chouseichan.dto.EventDisplayDto;
import jp.co.tis.tiw.chouseichan.entity.Candidate;
import jp.co.tis.tiw.chouseichan.entity.Vote;

class EventDisplayDtoJsonSerializerTest {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void serialize() throws Exception {
        EventDisplayDto eventDisplayDto = new EventDisplayDto();

        eventDisplayDto.setEventId(1);
        eventDisplayDto.setEventName("aaa");
        eventDisplayDto.setDescription("bbb");

        Candidate candidate1 = new Candidate();
        candidate1.setCandidateId(2);
        candidate1.setDateTime("ccc");
        Candidate candidate2 = new Candidate();
        candidate2.setCandidateId(3);
        candidate2.setDateTime("ddd");
        List<Candidate> columns = List.of(candidate1, candidate2);
        eventDisplayDto.setColumns(columns);

        DataSourceDto dataSourceDto1 = new DataSourceDto();
        dataSourceDto1.setName("eee");
        Vote vote1 = new Vote();
        vote1.setCandidateId(2);
        vote1.setAnswer("fff");
        Vote vote2 = new Vote();
        vote2.setCandidateId(3);
        vote2.setAnswer("ggg");
        dataSourceDto1.setVotes(List.of(vote1, vote2));

        DataSourceDto dataSourceDto2 = new DataSourceDto();
        dataSourceDto2.setName("hhh");
        Vote vote3 = new Vote();
        vote3.setCandidateId(2);
        vote3.setAnswer("iii");
        Vote vote4 = new Vote();
        vote4.setCandidateId(3);
        vote4.setAnswer("jjj");
        dataSourceDto2.setVotes(List.of(vote3, vote4));

        List<DataSourceDto> dataSource = List.of(dataSourceDto1, dataSourceDto2);
        eventDisplayDto.setDataSource(dataSource);

        String json = objectMapper.writeValueAsString(eventDisplayDto);

        JsonNode root = objectMapper.readTree(json);

        assertEquals(1, root.get("eventId").asInt());
        assertEquals("aaa", root.get("eventName").asText());
        assertEquals("bbb", root.get("description").asText());

        JsonNode columnsNode = root.get("columns");
        assertTrue(columnsNode.isArray());
        assertEquals(3, columnsNode.size());
        assertEquals("名前", columnsNode.get(0).get("title").asText());
        assertEquals("name", columnsNode.get(0).get("dataIndex").asText());
        assertEquals("name", columnsNode.get(0).get("key").asText());
        assertEquals("ccc", columnsNode.get(1).get("title").asText());
        assertEquals("2", columnsNode.get(1).get("dataIndex").asText());
        assertEquals("2", columnsNode.get(1).get("key").asText());
        assertEquals("ddd", columnsNode.get(2).get("title").asText());
        assertEquals("3", columnsNode.get(2).get("dataIndex").asText());
        assertEquals("3", columnsNode.get(2).get("key").asText());

        JsonNode dataSourceNode = root.get("dataSource");
        assertTrue(dataSourceNode.isArray());
        assertEquals(2, dataSourceNode.size());
        assertEquals(1, dataSourceNode.get(0).get("key").asInt());
        assertEquals("eee", dataSourceNode.get(0).get("name").asText());
        assertEquals("fff", dataSourceNode.get(0).get("2").asText());
        assertEquals("ggg", dataSourceNode.get(0).get("3").asText());
        assertEquals(2, dataSourceNode.get(1).get("key").asInt());
        assertEquals("hhh", dataSourceNode.get(1).get("name").asText());
        assertEquals("iii", dataSourceNode.get(1).get("2").asText());
        assertEquals("jjj", dataSourceNode.get(1).get("3").asText());
    }

    @Test
    void serialize_empty_columns_empty_datasource() throws Exception {
        EventDisplayDto eventDisplayDto = new EventDisplayDto();

        eventDisplayDto.setEventId(1);
        eventDisplayDto.setEventName("aaa");
        eventDisplayDto.setDescription("bbb");

        eventDisplayDto.setColumns(Collections.emptyList());
        eventDisplayDto.setDataSource(Collections.emptyList());

        String json = objectMapper.writeValueAsString(eventDisplayDto);

        JsonNode root = objectMapper.readTree(json);

        assertEquals(1, root.get("eventId").asInt());
        assertEquals("aaa", root.get("eventName").asText());
        assertEquals("bbb", root.get("description").asText());

        JsonNode columnsNode = root.get("columns");
        assertTrue(columnsNode.isArray());
        assertEquals(1, columnsNode.size());
        assertEquals("名前", columnsNode.get(0).get("title").asText());
        assertEquals("name", columnsNode.get(0).get("dataIndex").asText());
        assertEquals("name", columnsNode.get(0).get("key").asText());

        JsonNode dataSourceNode = root.get("dataSource");
        assertTrue(dataSourceNode.isArray());
        assertEquals(0, dataSourceNode.size());
    }
}