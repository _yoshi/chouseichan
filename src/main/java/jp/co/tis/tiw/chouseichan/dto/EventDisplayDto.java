package jp.co.tis.tiw.chouseichan.dto;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import jp.co.tis.tiw.chouseichan.entity.Candidate;
import jp.co.tis.tiw.chouseichan.entity.Event;
import jp.co.tis.tiw.chouseichan.json.EventDisplayDtoJsonSerializer;

@JsonSerialize(using = EventDisplayDtoJsonSerializer.class)
public class EventDisplayDto extends Event {

    private List<Candidate> columns = Collections.emptyList();

    private List<DataSourceDto> dataSource = Collections.emptyList();

    public List<Candidate> getColumns() {
        return columns;
    }

    public void setColumns(List<Candidate> columns) {
        this.columns = columns;
    }

    public List<DataSourceDto> getDataSource() {
        return dataSource;
    }

    public void setDataSource(List<DataSourceDto> dataSource) {
        this.dataSource = dataSource;
    }
}
