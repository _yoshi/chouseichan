package jp.co.tis.tiw.chouseichan.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.tis.tiw.chouseichan.dto.EventDisplayDto;
import jp.co.tis.tiw.chouseichan.entity.Event;
import jp.co.tis.tiw.chouseichan.form.EventEntryForm;
import jp.co.tis.tiw.chouseichan.form.ParticipantEntryForm;
import jp.co.tis.tiw.chouseichan.service.EventService;

import java.util.List;

@RestController
@RequestMapping("/events")
public class EventController {

    private final EventService service;

    /**
     * コンストラクタ.
     * @param service イベントサービス
     */
    public EventController(EventService service) {
        this.service = service;
    }

    /**
     * イベントを作成する。
     *
     * @param form 作成するイベント
     * @return 作成されたイベント
     */
    @PostMapping
    public Event create(@Validated @RequestBody EventEntryForm form) {
        List<String> dateTimes =  form.getCandidateDateTimes();
        Event newEvent = service.registerNewEvent(form.getEventName(), form.getDescription(), dateTimes);
        return newEvent;
    }

    /**
     * イベントを取得する。
     * 対応するイベントが存在しない場合は、404 NOT FOUNDが返却される。
     *
     * @param eventId イベントID
     * @return イベントを格納したレスポンス。
     */
    @GetMapping("/{id}")
    public ResponseEntity<EventDisplayDto> getEvent(@PathVariable("id") Integer eventId) {
        EventDisplayDto dto = service.getEvent(eventId);
        if (dto == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(dto);
    }

    /**
     * 参加可否を登録する。
     * @param form 参加可否
     */
    @PostMapping("/{id}")
    public void register(@Validated @RequestBody ParticipantEntryForm form) {
        service.registerNewParticipant(form.getParticipantName(), form.getComment(), form.getVoteForms());
    }
}